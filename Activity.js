
/**
    1.class
    2.class name should be nouns,in mixed  case with the first letter of each internaL word capitilized
    3.this process is know as instantiation
    4.constructor function with used of keyword new   
    5.Contructor 

 */


// Activity # 1
class Student {
    constructor(name, grades, email) {
        const isBelowThreshold = (currentValue) => currentValue < 100 || currentValue >= 0 || currentValue === undefined;
        this._name = name;
        this._grades = grades.every(isBelowThreshold) ? grades : undefined
        this._email = email;

    }

}

let student = new Student("john", [90, 84, 78, 88], "john@gmail.com");
console.log(student);

//

class StudentOne {
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.grades = grades;

    }
}

let studentOne = new StudentOne("john", [90, 84, 78, 88], "john@gmail.com");
console.log(studentOne);
let studentTwo = new StudentOne("joe", [78, 82, 79, 85], "john@gmail.com");
console.log(studentTwo);
let studentThree = new StudentOne("john", [87, 82, 79, 85], "john@gmail.com");
console.log(studentThree);
let studentFour = new StudentOne("john", [91, 89, 92, 93], "john@gmail.com");
console.log(studentFour);



// Activity # 2


class StudentNew {
    constructor(name, grades, email) {
        this._name = name;
        this._gradAve = undefined;
        this._email = email;
        this._grades = grades;
        this._passed = undefined;
        this._passedWitHonor = undefined;
    }

    login() {
        console.log(`${this._name} is logged in`);
        return this
    }
    logout() {
        console.log(`${this._name} is loggout in`);
        return this

    }


    computeAverage() {
        let sum = 0;
        this._grades.forEach(element => {
            sum = sum + element
        });
        this._gradAve = sum / 4;
        return this

    }
    willPass() {
        let x = this.computeAverage() >= 85 ? true : false;
        this._passed = x;
        return this;



    }

    willPassWithHonors() {
        if (this.willPass()) {
            if (this.computeAverage() >= 90) {
                return true
            } else {
                this._passedWitHonor = false
            }
        } else {
            this._passedWitHonor = false
        }
        return this


    }

}

let studentNew = new StudentNew("john", [89, 78, 84, 88], "john@gmail.com")



// quiz 
/**
 1. no
 2.no
 3.yes
 4. getters and setters
 5.this

 * 
 * 
 */